import {RequestController, requestController} from '../services/dialogflow-service.js'

const request = new RequestController();
await request.initConnection();
let conversationData = []

export async function sendRequest (req, res) {
    res.render('main');
}

export async function sendQuery (req, res) {
    let answer = await requestController.runQuery(req.body.requestdata, request);
    if(conversationData.length > 14){
        conversationData.shift();
        conversationData.shift();
    }
    conversationData.push(req.body.requestdata);
    conversationData.push(answer[0].queryResult.fulfillmentText);
    res.render('main', { data: conversationData });
}
