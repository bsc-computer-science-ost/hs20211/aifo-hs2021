import express from 'express';
const router = express.Router();
import * as randomController from '../controllers/random-controller.js'

router.get('/', randomController.sendRequest);
router.post('/', randomController.sendQuery)

export const requests = router;
