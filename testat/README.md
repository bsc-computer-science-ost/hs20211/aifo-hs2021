# AI Foundations Testat
This repository contains the files for our testat hand in int he OST module
AI Foundations.

## 1. Introduction
In the current situation with the pandemic ongoing we want to make it possible for people to order drinks in a bar completely without any bartenders. Our idea is to write a chat bot where you can order drinks, food and liquors like in a normal bar.
This work does demonstrate a first proof of concept. This work can then be used to determine if the solution does fulfill all requirement to be used in the first bar without a bartender visible. For sure there will be a bartender in the background making the drinks. In a further step an automatic bartender could be defined, but that is out of scope for this project.
## 1.1 Goal
The Customer (User) wants to be able to order a drink from a tablet that sits on every table. The Customer doesn’t need to read a menu to know what drinks the bar can offer, he can just re-quest whatever he wants and if it is known to the bot, i.e., the bar provides it, the request gets fulfilled.
## 1.2 Tech Stack
Our tech stack consists of google dialogflow and JavaScript for the web application we want to use. The web Application will make requests to the API service of dialogflow to analyze the questions of the user and give an appropriate answer. In addition, after a submitted request the staff or the automatic bartender will get the order to fulfill the customer needs.